// Create

db.users.insertMany([
	{
		"firstName" : "John",
		"lastName" : "Lennon",
		"email" : "jlennon1940@gmail.com",
		"password" : "imagine80",
		"isAdmin" : false
	},
	{
		"firstName" : "Paul",
		"lastName" : "McCartney",
		"email" : "paulisAlive@gmail.com",
		"password" : "paulYesterday64",
		"isAdmin" : false
	},
	{
		"firstName" : "George",
		"lastName" : "Harrison",
		"email" : "somethingGeorge@gmail.com",
		"password" : "iamTheSun",
		"isAdmin" : false
	},
	{
		"firstName" : "Richard",
		"lastName" : "Starsky",
		"email" : "ringoStarr@gmail.com",
		"password" : "yellowSubmarine",
		"isAdmin" : false
	},
	{
		"firstName" : "Pete",
		"lastName" : "Best",
		"email" : "pbBeatles@gmail.com",
		"password" : "peteTheBest",
		"isAdmin" : false
	}
])


db.courses.insertMany([
	{
		"name" : "Physics 1",
		"price" : 1000,
		"isActive" : false
	},
	{
		"name" : "Psychology",
		"price" : 2000,
		"isActive" : false
	},
	{
		"name" : "Programming 101",
		"price" : 3000,
		"isActive" : false
	}
])


// Read

db.users.find({"isAdmin" : false})

// Update

db.users.updateOne({}, {$set:{"isAdmin" : true}})

db.courses.updateOne({"name" : "Physics 1"}, {$set:{"isActive" : true}})

// Delete 

db.courses.deleteMany({"isActive" : false})
